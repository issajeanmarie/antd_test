import React, { useState } from 'react'
import { Farmers, FarmerNav, Model } from '../../component'

//Change data based on the search

const Farmer = () => {

    const theData = localStorage.getItem('farmers')
    const allData = JSON.parse(theData) || []
    const [data, setData] = useState( allData || []);

    const getData = e => {
        const theData = localStorage.getItem('farmers')
        const allData = JSON.parse(theData) || []
        const newData = [...allData, e];
        setData(newData)
    }

    const deleteFarmer = index => {
        const newData = data.filter(d => d.index !== index)
        localStorage.setItem('farmers', JSON.stringify(newData))
        setData(newData)
    }

    const getSearch = e => {
        let oldFarmers = JSON.parse(localStorage.getItem('farmers')) || [];
        let newFarmers = 
            oldFarmers.filter(farmer => 
                farmer.fname.toLowerCase().includes(e.toLowerCase()) ||
                farmer.lname.toLowerCase().includes(e.toLowerCase())
            )
        setData(newFarmers)
    }

    return(
        <>
            <FarmerNav getSearch={ getSearch } />
            <Farmers allData={ data } deleteData={ deleteFarmer } />
            <Model sendData={ getData } />
        </>
    )
}

export default Farmer;