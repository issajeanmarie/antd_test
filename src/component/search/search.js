import React, { useState } from 'react'
import './index.css'
import { 
    FilterOutlined, 
    SearchOutlined, 
    SortAscendingOutlined 
} from '@ant-design/icons/lib/icons'

const Search = ({ getSearch }) => {

	const [search, setSearch] = useState('')
	const handleChange = e => {
		getSearch(e.target.value)
		setSearch(e.target.value)
	}

    return(
        <div className="searchBox">
			<span><SearchOutlined /></span>
			<input 
				data-testid="search-input"
				type="text" 
				placeholder="Search" 
				value={search}
				onChange={handleChange} 
			/>

			<div className="filters" style={{paddingTop: '13px', fontSize: '0.97em'}}>
				<p><SortAscendingOutlined /> Sort</p>
				<p><FilterOutlined /> Filter</p>
			</div>	
		</div>
    )
}

export default Search