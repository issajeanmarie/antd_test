import React from 'react'
import { Link } from 'react-router-dom'
import './index.css'
import { 
    LayoutOutlined, 
    EnvironmentOutlined ,
    UserOutlined,
    BuildFilled
} from '@ant-design/icons/lib/icons'
import { Layout } from 'antd/lib';
const {  Sider } = Layout;

const Sidemenu = ({ isCollapsed }) => (
    <>
     <Sider 
        collapsible collapsed={ isCollapsed } 
        className={`bg_black sidebar`}>

        {!isCollapsed ? <h1>Dashboard</h1> : null}
			
        <Link to="/overview">
            <li>
                <span className="icon"><LayoutOutlined /></span>
                {!isCollapsed ? 'Overview' : null}
            </li>
        </Link>

        <Link to="/farmer">
            <li className='active'>
                <span className="icon"><BuildFilled /></span>
                {!isCollapsed ? 'Farmers' : null}
            </li>
        </Link>

        <li>
            <span className="icon"><UserOutlined /></span>
            {!isCollapsed ? 'Account' : null}
        </li>

        <li>
            <span className="icon"><EnvironmentOutlined /></span>
            {!isCollapsed ? 'Zones' : null}
        </li>

        <p className="powered">Powered by <a href="https://awesomity.rw/">Awesomity Lab</a></p>
        </Sider>
    </>
)

export default Sidemenu