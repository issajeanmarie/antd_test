import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Toggle from '../toggle'

let container = null
beforeEach(() => {
    //Prepare
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    //Clean up
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

describe("Toggle button", () => {
    it("renders correctly", () => {
        act(() => {
            render(<Toggle />, container)
        })
        expect(container.textContent).toBeFalsy()  
        
        act(() => {
            render(<Toggle />, container)
        })
        expect(container).toBeTruthy()
    })
})