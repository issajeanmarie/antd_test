import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Navbar from '../navbar'

let container = null
beforeEach(() => {
    //Prepare
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    //Clean up
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

describe("Navbar", () => {
    it("renders correctly", () => {
        act(() => {
            render(<Navbar />, container)
        })
        expect(container).toBeTruthy()
    })
})