import React from 'react'
// import Toggle from './toggle'
import './index.css'
import Awesomity from '../../assets/img/IB_logo.png'
import Profile from './profile'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
 
} from '@ant-design/icons';
import { Layout } from 'antd/lib';

const { Header } = Layout;
const Navbar = ({ toggle, isCollapsed }) => {
    
    return(
        <>
        <Header className="bg_white navbar" data-testid="whole-navbar">
            <span className="trigger" onClick={toggle}>
                {isCollapsed?<MenuUnfoldOutlined/>:<MenuFoldOutlined/>}
            </span>
           
            <img className="awesomity" src={Awesomity} alt="" />
            <p 
                className="title_4 text_white_green"
                style={{paddingTop: '10px'}}
            >
                Awesomity Lab
            </p>
            <Profile />
            </Header>
        </>
    )
}
export default Navbar