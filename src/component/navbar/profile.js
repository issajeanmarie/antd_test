import React from 'react';
import Picture from '../../assets/img/Logo.jpg';
import { CaretDownOutlined, BellFilled } from '@ant-design/icons/lib/icons'

function Profile() {
	return (
		<div className="profile" data-testid="profile-cont">
			<span className="notfication" style={{lineHeight: '25px'}}>
                <BellFilled />
            </span>
			<div className="imgCont">
				<img src={Picture} alt="" />
			</div>
			<p style={{paddingTop: '10px'}}>
				Issa Jean Marie
			</p>
			<span style={{fontSize: '1em', cursor: 'pointer'}}>
                <CaretDownOutlined />
            </span>
            {/* It worked */}
		</div>
	)
}

export default Profile