import React, { useState } from 'react'

function Toggle({ toggleStatus }) {

	const [state, setState] = useState(true);

	const toggleMenu = () => {
		setState(!state);
        toggleStatus(state)
	}

	return (
		<div data-testid="toggleBtn" className="toggle" onClick={ toggleMenu }>
			<div className="line"></div>
		</div>
	)
}

export default Toggle