import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { Button } from '../'
import { act } from 'react-dom/test-utils'

let container = null;
beforeEach(() => {
    //Prepare the rendering area
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    //Clean up area after each test
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

describe("Button", () => {
    it("Renders  without crashing", () => {
        act(() => {
            render(<Button text='Button' />, container)
        })
        expect(container.textContent).toBe('Button')
    
        act(() => {
            render(<Button text="Changed Button" />, container)
        })
        expect(container.textContent).toBe("Changed Button")
    })
})