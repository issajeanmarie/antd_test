import React from 'react'

function Button({text, call}) {

	return (
			<button
				data-testid="btn-primary"
				onClick={() => call !== undefined ? call() : null} 
				className="btn_primary"
			>
				{text}
			</button>
	)
}

export default Button