import { Table, Space } from 'antd/lib';
import './index.css'


const Farmers = ({ deleteData, allData }) => {

    const { Column } = Table;

    const deleteFarmer = record => deleteData(record.index)

    return(
        <div className="theTable">
          
          <h1>Farmers ({allData.length})</h1>

          <Table dataSource={allData} key={allData.index}>
            <Column dataIndex="fname" key="index" />
            <Column dataIndex="lname" key="index" />
            <Column dataIndex="spaunse" key="index" />
            <Column 
              dataIndex='dob' 
              key="index" 
            />
            <Column
                key="index"
                render={(text, record) => (
                  <Space size="small">
                    <span style={{color: 'black', cursor: 'pointer'}}>View</span>
                    <span style={{color: 'black', cursor: 'pointer'}}>Edit</span>
                    <span style={{color: 'black', cursor: 'pointer'}}>Deactivate</span>
                    <span style={{cursor: 'pointer'}} className='text_red' onClick={() => deleteFarmer(record)}>Delete</span>
                  </Space>
                )}
            />
            </Table>
        </div>
    )
}

export default Farmers