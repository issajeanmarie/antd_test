import React from 'react'
import { Button } from '../'
import { Search } from '../'

const FarmerNav = ({ getSearch }) => {

     const openModel = () => document.getElementById('model').classList.remove('hide')

    return (
        <div className="Searchbar">
            <Search getSearch={getSearch} />
            <Button call={openModel} text="ADD FARMER" />
        </div> 
    )
}

export default FarmerNav