import React, { useState } from 'react'
import ReactDOM from 'react-dom'
// import { Form, Input, SubmitButton } from 'formik-antd'
import { CloseOutlined } from '@ant-design/icons'
import { setIn, useFormik } from 'formik'
import { Button } from '../'
import * as Yup from 'yup'
 
function Model({ sendData }) {

    const closeModel = () => document.getElementById('model').classList.add('hide')

    const [index, setIndex] = useState(Date.now())
    const [data, setData] = useState([])

    const formik = useFormik({
        initialValues: {
            index: index,
            fname: '',
            lname: '',
            gender: 'Female',
            dob: '',
            martalStatus: 'Single',
            dependants: '',
            spaunse: 'None'
        },
        validationSchema: Yup.object({
            fname: Yup.string().required('Required'),
            lname: Yup.string().required('Required'),
            dependants: Yup.number(),
            spaunse: Yup.string()
        }),
        onSubmit: values => {
           sendData(values)
           closeModel()

           //Current data 
           const theData = JSON.parse(localStorage.getItem('farmers')) || data
           const newData = [...theData, values]
           localStorage.setItem('farmers', JSON.stringify(newData))
           setData(newData)
           setIndex(Date.now())
           
           return (
                values.fname = '',
                values.lname = '',
                values.gender = 'Female',
                values.dob = '',
                values.martalStatus = 'Single',
                values.dependants = '',
                values.spaunse = 'None'
           )
        }
    })

    return ReactDOM.createPortal(        

        <div className="Newmember">
            <p className="close-btn" onClick={closeModel} style={{paddingTop: '10px'}}>
				<CloseOutlined />
			</p>

            <h1 className="title_2">Add new Member</h1>
			<p className="title_4">Personal information</p>

            <form onSubmit={formik.handleSubmit}>
                <div className="input-group">
                    <label>
                        First name 
                        {formik.touched.fname && formik.errors.fname ?
                        <span style={{color: 'maroon'}}>({formik.errors.fname})</span> : null}
                    </label>
                    <input 
                        {...formik.getFieldProps('fname')}
                        name="fname" 
                        type="text" 
                        placeholder="First name"
                    />
                </div>                

                <div className="input-group">
                    <label>
                        Last name 
                        {formik.touched.lname && formik.errors.lname ? 
                        <span style={{color: 'maroon'}}>({formik.errors.lname})</span> : null}
                    </label>
                    <input 
                        {...formik.getFieldProps('lname')}
                        name="lname" 
                        type="lname" 
                        placeholder="Last name" 
                    />
                </div>

                <div className="input-group small">
                    <label>Gender</label>
                    <select     
                        {...formik.getFieldProps('gender')}
                        name="gender" 
                    >
                        <option>Female</option>
                        <option>Male</option>
                    </select>
                </div>

                <div className="input-group small">
                    <label>
                        Date of birth 
                        {formik.touched.dob && formik.errors.dob ? 
                        <span style={{color: 'maroon'}}>({formik.errors.dob})</span> : null}
                    </label>
                    <input 
                        {...formik.getFieldProps('dob')} 
                        name="dob" 
                        type="date" 
                        placeholder="DOB" 
                    />
                </div>

                <div className="input-group small">
                    <label>Marital status</label>
                    <select     
                        {...formik.getFieldProps('martalStatus')}
                        name="martalStatus" 
                    >
                        <option>Single</option>
                        <option>Maried</option>
                    </select>
                </div>

                <div className="input-group small">
                    <label>
                        Number of dependents 
                        {formik.touched.dependants && formik.errors.dependants ? 
                        <span style={{color: 'maroon'}}>({formik.errors.dependants})</span> : null}
                    </label>
                    <input 
                        {...formik.getFieldProps('dependants')} 
                        name="dependants" 
                        type="number" 
                        placeholder="00" 
                    />
                </div>

                <div className="input-group small">
                    <label>
                        Spaunse's name
                    </label>
                    <input 
                        {...formik.getFieldProps('spaunse')}
                        name="spaunse" 
                        type="text" 
                        placeholder="Full name" 
                    />
                </div>

                <div className="flexFix"></div>
			    <br />

                <Button text="ADD FARMER" />
            </form>

        </div>,
        document.getElementById('model')
    )
}

export default Model