import React, { useState } from 'react'
import { Farmer } from '../views'
import { Sidemenu } from '../component'
import { Navbar } from '../component'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import { Layout } from 'antd/lib'
const { Content } = Layout

const Routes = () => {    
    
    const [isCollapsed,setIsCollapsed]=useState(false)
    // Change state to show/hide the sidebar menu
    const toggle=()=>{
        setIsCollapsed(!isCollapsed)
    }

    return(
        <BrowserRouter>
            <Layout data-testid="layout">           
                <Sidemenu toggle={toggle} isCollapsed={isCollapsed}/> {/* Exported from the component directory */}         
                <Layout>                
                    <Navbar toggle={toggle} isCollapsed={isCollapsed} /> {/* Exported from the component directory */}                         
                    <Content className="pad020">
                        <Switch>
                            <Route path="/farmer" exact component={Farmer} />
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        </BrowserRouter>
    )
};

export default Routes