# Farmers Project

## Installation process

1. Clone repository
2. Run __yarn install__ command, to install dependencies
3. Run __yarn start__ command, to start the server

## installed packages
  1. React antd
  2. Formik
  3. Yup
  4. cra-template
  5. craco-less
  6. @ant-design/icons